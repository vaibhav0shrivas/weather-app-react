import { Component } from "react";
import "../assets/styles/FiveDaysWeatherForecast.css"

class FiveDaysWeatherForecast extends Component {
    constructor(props) {
        super(props);

        this.state = {
            latitude: 34.0522,
            longitude: 118.2437,
            city: "Suqian",
            country: "CN",
            isLocationGranted: false,
            isWeatherDataAvailable: false,
            fetchFailed: null,
            weatherData: null,
            showLoader: false,
            locationPermissionDenied: false,
        }

        this.API_key = '074cfb46165e7b23dd4e54c943a1d145';

    };

    locateMe = () => {

        let lat = null;
        let lon = null;

        this.setState({
            locationPermissionDenied: false,

        },
            () => {
                if ("geolocation" in navigator) {

                    navigator.geolocation.getCurrentPosition((position) => {

                        console.log("Location Available");

                        lat = position.coords.latitude;
                        lon = position.coords.longitude;

                        this.setState({
                            latitude: lat,
                            longitude: lon,
                            city: null,

                        },
                            () => {
                                this.fetchFiveDayForeCast(lat, lon);
                            }
                        );

                    },
                        (error) => {
                            if (error.code == error.PERMISSION_DENIED) {
                                console.log("before set state", this.state);
                                this.setState({
                                    locationPermissionDenied: true,
                                },
                                    () => {
                                        console.log("Location Unavailable", this.state);
                                    }
                                );

                            }

                        }
                    );

                }

            }
        );


    };

    generalWeatherData = (passedWeatherData) => {

        let updatedWeatherData = Object.entries(passedWeatherData).map((element) => {

            return {
                key: element[1].dt,
                date_str: element[0],
                temp: element[1].main.temp,
                feels_like: element[1].main.feels_like,
                temp_max: element[1].main.temp_min,
                temp_min: element[1].main.temp_max,
                main: element[1].weather[0].main,
                description: element[1].weather[0].description,
                icon: element[1].weather[0].icon,
                weatherIconURL: `http://openweathermap.org/img/wn/${element[1].weather[0].icon}@2x.png`

            }
        });

        return updatedWeatherData;

    }

    fetchFiveDayForeCast = (latitude, longitude) => {
        let lang = 'en';
        let units = 'metric';
        let url = `https://api.openweathermap.org/data/2.5/forecast/?lat=${latitude}&lon=${longitude}&appid=${this.API_key}&units=${units}&lang=${lang}`;

        this.setState({
            fetchFailed: null,
            showLoader: true,
            isWeatherDataAvailable: false
        },
            () => {
                console.log("fetch called with lat/long ", latitude, "/", longitude);
                fetch(url)
                    .then((response) => {
                        if (!response.ok) {
                            console.log("Response not okay")
                            throw new Error(response.statusText);

                        } else {
                            console.log("Response okay")
                            return response.json();
                        }

                    })
                    .then((data) => {
                        let timeStamp = data.list[0].dt_txt.slice(-8);

                        let weatherData = data.list.reduce((accumulator, entry,) => {
                            if (entry.dt_txt.split(" ")[0] in accumulator === false) {
                                accumulator[entry.dt_txt.split(" ")[0]] = entry;

                            } else if (entry.dt_txt.slice(-8) === timeStamp) {
                                accumulator[entry.dt_txt.split(" ")[0]] = entry;

                            }


                            return accumulator;

                        },
                            {

                            }
                        );

                        this.setState({
                            isWeatherDataAvailable: true,
                            city: data.city.name,
                            country: data.city.country,
                            latitude: latitude,
                            longitude: longitude,
                            showLoader: false,
                            fetchFailed: false,
                            weatherData: this.generalWeatherData(weatherData),
                        });
                    })
                    .catch((error) => {
                        console.error(error);
                        this.setState({
                            fetchFailed: true,
                            showLoader: false,
                            city: null,
                        });

                    });
            }
        );

    };

    componentDidMount() {
        this.fetchFiveDayForeCast(this.state.latitude, this.state.longitude);
    }

    render() {
        return (
            <div className="weather-forecast-container">
                {
                    this.state.city !== null &&
                    <h2 className="location-name">
                        {this.state.city}, {this.state.country}
                    </h2>
                }
                {
                    this.state.locationPermissionDenied === true &&
                    <h4 className="location-name">
                        You denied location permission, showing default location!
                    </h4>
                }
                {
                    this.state.showLoader === true && this.state.city === null &&
                    <h2 className="location-name">
                        Getting your location data..
                    </h2>
                }
                <span>
                    Latitude : {this.state.latitude}, Longitude : {this.state.longitude}
                </span>
                <div>
                    <button onClick={this.locateMe}
                        title="Press to let us know your location">Locate Me</button>
                </div>
                <div className="main-content">
                    {
                        this.state.showLoader === true &&
                        <div class="loader"></div>
                    }

                    {
                        this.state.isWeatherDataAvailable === true &&
                        <ul>

                            {this.state.weatherData.map((dailyData) => {
                                return (
                                    <li className="card"
                                        key={dailyData.key}>
                                        <h3>{new Date(dailyData.date_str).toString().slice(0, 15)}</h3>
                                        <img width="40px"
                                            height="40px"
                                            src={dailyData.weatherIconURL}
                                            alt={dailyData.description} />
                                        <p>Weather : {dailyData.main}</p>
                                        <p>Temperature : {dailyData.temp}&deg; C</p>
                                        <p>Feels Like : {dailyData.feels_like}&deg; C</p>
                                        <p>Minimum : {dailyData.temp_min}&deg; C</p>
                                        <p>Maximum : {dailyData.temp_max}&deg; C</p>
                                    </li>
                                );

                            })}
                        </ul>
                    }
                    {
                        this.state.fetchFailed === true && this.showLoader === false &&
                        <div className="error-div">
                            <h2>Sorry we failed to fetch weather data!</h2>
                        </div>

                    }
                    {
                        this.state.isWeatherDataAvailable === false && this.showLoader === false &&

                        <div className="error-div">
                            <h2>Sorry weather data unavailable, try again later!</h2>
                        </div>

                    }

                </div>

            </div>
        );
    };



};

export default FiveDaysWeatherForecast;